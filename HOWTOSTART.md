The project consists of two classes: 
- Division
- App

The Division class contains the logic, which takes care of the correct division and the App class is used to run the project.

The variables called money and people need to be filled in order to run the project correctly. They are of type BigDecimal, which expect output in form XX.XX and XX.

To run the program, import it to an IDE of your choice and edit the variables money and people to your liking.
