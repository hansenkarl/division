import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Division {
	private BigDecimal money;
	private BigDecimal people;
	private BigDecimal difference = BigDecimal.valueOf(0.01);
	private boolean extraMoneyGivenOut = false;

	public Division(BigDecimal money, BigDecimal people) {
		if (people == null) {
			System.out.println("Since field people received a value of null, it is set to the value of 0!");
			people = BigDecimal.ZERO;
		}
		if (money == null) {
			System.out.println("Since field money received a value of null, it is set to the value of 0!");
			money = BigDecimal.ZERO;
		}
		this.money = money.setScale(2, RoundingMode.HALF_UP);
		this.people = people.setScale(0, RoundingMode.HALF_UP);
	}

	public BigDecimal getMoney() {
		return this.money;
	}

	public BigDecimal getPeople() {
		return this.people;
	}

	public List<BigDecimal> divideMoney() {
		if (people.compareTo(BigDecimal.ZERO) <= 0) {
			System.out.println("No people to divide money to!");
			return new ArrayList<>();
		}
		if (money.compareTo(BigDecimal.ZERO) == 0) {
			System.out.println("No money to divide!");
			return new ArrayList<>();
		}
		BigDecimal moneyForOne = (money.divide(people, 2, RoundingMode.DOWN)).max(BigDecimal.valueOf(0.01));
		List<BigDecimal> divided = new ArrayList<>();

		for (BigDecimal i = BigDecimal.ZERO; i.compareTo(people) < 0; i = i.add(BigDecimal.ONE)) {
			divided.add(moneyForOne);
		}

		return calculateInitiallyShared(divided, money);
	}

	private List<BigDecimal> calculateInitiallyShared(List<BigDecimal> initialDivided, BigDecimal money) {
		BigDecimal initialShared = BigDecimal.ZERO;
		for (BigDecimal moneyForOne : initialDivided) {
			initialShared = initialShared.add(moneyForOne);
		}

		return getCorrectDivision(initialDivided, money, initialShared);
	}

	private List<BigDecimal> getCorrectDivision(List<BigDecimal> resultToFix, BigDecimal moneyTotal,
			BigDecimal initalShared) {
		BigDecimal valuesToEdit = getValuesToedit(moneyTotal, initalShared);

		int i = 0;

		if (extraMoneyGivenOut) {
			Collections.reverse(resultToFix);
		}
		while (valuesToEdit.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal resultToEdit = resultToFix.get(i);
			BigDecimal fixedResult = BigDecimal.ZERO;
			fixedResult = (extraMoneyGivenOut ? resultToEdit.subtract(difference) : resultToEdit.add(difference));
			resultToFix.remove(i);
			resultToFix.add(i, roundToSecondDecimalPoint(fixedResult));
			i += 1;
			valuesToEdit = valuesToEdit.subtract(BigDecimal.ONE);
		}
		if (extraMoneyGivenOut) {
			Collections.reverse(resultToFix);
		}

		return resultToFix;
	}

	private BigDecimal getValuesToedit(BigDecimal moneyTotal, BigDecimal initalShared) {
		BigDecimal valuesToEdit = BigDecimal.ZERO;
		BigDecimal toDistribute = BigDecimal.ZERO;
		if (initalShared != moneyTotal) {
			toDistribute = moneyTotal.subtract(initalShared);
			if (toDistribute.compareTo(BigDecimal.ZERO) < 0) {
				toDistribute = toDistribute.abs();
				extraMoneyGivenOut = true;
			}
			while (toDistribute.compareTo(BigDecimal.ZERO) > 0) {
				valuesToEdit = valuesToEdit.add(BigDecimal.ONE);
				toDistribute = roundToSecondDecimalPoint(toDistribute.subtract(difference));
			}
		}

		return valuesToEdit;
	}

	private BigDecimal roundToSecondDecimalPoint(BigDecimal number) {
		return number.setScale(2);
	}
}
