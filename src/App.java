import java.math.BigDecimal;

public class App {
    public static void main(String[] args) throws Exception {
        BigDecimal money = BigDecimal.valueOf(10);
        BigDecimal people = BigDecimal.valueOf(7);
        Utils.printResultToConsole(money, people);
    }
}
