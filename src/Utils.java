import java.util.List;
import java.math.BigDecimal;

class Utils {
	public static void printResultToConsole(BigDecimal money, BigDecimal people) {
		Division div = new Division(money, people);
        List<BigDecimal> divided = div.divideMoney();
        BigDecimal total = BigDecimal.ZERO;

        for (BigDecimal moneyToOne : divided) {
            total = total.add(moneyToOne);
        }
        
        System.out.println();
        System.out.println("Amount of money: " + div.getMoney());
        System.out.println("Amount of people: " + div.getPeople());
        System.out.println("How the dividing looks like:\n" + divided);
        System.out.println();
        System.out.println("managed to divide: " + total);
        System.out.println();
	}
}
